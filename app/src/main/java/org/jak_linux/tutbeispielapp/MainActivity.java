package org.jak_linux.tutbeispielapp;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivityTag";

    on

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String beispiel = getString(R.string.hello_universe);

        final EditText myTextView = (EditText) findViewById(R.id.my_textview);

        myTextView.setText(beispiel);
        myTextView.setText(R.string.hello_universe);

        final Button button = (Button) findViewById(R.id.button);

        button.setOnLongClickListener(new Button.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(MainActivity.this, "Du hast lange geklickt", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, beispiel, Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.activity_main).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Du hast irgendwo ins Fenster geklickt", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onClick: Es wurde ins fenster geklickt");
            }
        });

        myTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (myTextView.length() > 3) {
                    button.setBackgroundColor(Color.GREEN);
                    button.setEnabled(true);
                } else {
                    button.setBackgroundColor(Color.RED);
                    button.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        button.setEnabled(true);
        button.setBackgroundColor(Color.RED);


    }
}
